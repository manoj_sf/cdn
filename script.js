(function(){
		var formTags = document.getElementsByTagName('form');
			var allForms = Array.prototype.slice.call(formTags);
			var myForm = allForms[0];
			var inputs = myForm.getElementsByTagName('input');
			var submitInput, normalInput =[];
			var submitWords = ['submit', 'send', 'contact', 'connect', 'talk'];
			var rex = new RegExp(submitWords.join('|'));
		function run(){
			for(let i=0; i < inputs.length; i++){
				var input = inputs[i];
				if(input.type == "submit" || (input.type == "button" && rex.test(input.value.toLowerCase()))){
					input.addEventListener('click', submitItToSalesSimplify);
					submitInput = input;
				} else {
					normalInput.push(input);
				}
			}
			Array.prototype.slice.call(myForm.getElementsByTagName('textarea')).forEach(e => normalInput.push(e));
		}
		function submitItToSalesSimplify(){
			const op = {};
			normalInput.forEach(function(i) {
				if(i.id){
					op[i.id]=i.value;
				} else if (i.name) {
					op[i.name]=i.value;
				}
				op['yourUrl'] = window.location.href;
			})
			alert(JSON.stringify(op))
		}
		run();
	  })();
